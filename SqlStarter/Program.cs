﻿using ConsoleStorage.Command;
using System;
using System.Threading.Tasks;

namespace SqlStarter
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Executor executor = new Executor();

            //executor.SetCommands(new SqlCommands());
            executor.SetCommands(new SqlPhysics());

            if (args.Length > 0)
                await executor.RunCommand(args);

            await executor.Run();
        }
    }
}
