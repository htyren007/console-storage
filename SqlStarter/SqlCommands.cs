﻿using ConsoleStorage.Command;
using SqlManager;
using System;

namespace SqlStarter
{
    public class SqlCommands : ACommandCollection
    {
        private DataBaseAdapter db;
        private readonly string fileName = "console.sqlite";

        public SqlCommands()
        {
            commands.Add(new SimpleCommand("quit exit q", ()=> IsWork = false));
            commands.Add(new SimpleCommand("create", CreateTableTest));
            commands.Add(new SimpleCommand<string, string>("insert", InsertTest, (q) => q, (q)=> q));
            commands.Add(new SimpleCommand("all", SelectAll));
            //commands.Add(new SimpleCommand<string>("table", CreateTableTest));

            db = new DataBaseAdapter(fileName);
            if (!db.Exists)
                db.Create();
        }

        private void InsertTest(string author, string book)
        {
            string query = $"INSERT INTO Catalog ('author', 'book') values('{author}', '{book}')";
            db.ExecuteNonQuery(query);
        }

        private void SelectAll()
        {
            var table = db.Execute("SELECT * FROM Catalog");
            for (int i = 0; i < table.Rows.Count; i++)
            {
                System.Data.DataRow row = table.Rows[i];

                for (int j = 0; j < row.ItemArray.Length; j++)
                {
                    Console.Write(row[j]);
                    Console.Write('|');
                }
                Console.WriteLine();
            }
        }

        private void CreateTableTest()
        {
            db.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS Catalog(id INTEGER PRIMARY KEY AUTOINCREMENT, author TEXT, book TEXT)");
            
            
        }


        public override void WriteNotify()
        {
            Console.Write("Введите команду sql: ");
        }
    }
}