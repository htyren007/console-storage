﻿using ConsoleStorage.Command;
using ConsoleStorage.Utility;
using SqlManager;
using System;
using System.Diagnostics;
using System.Text;

namespace SqlStarter
{
    // https://tproger.ru/articles/5-zadanij-po-sql-s-realnyh-sobesedovanij/
    // SQLiteStudio
    public class SqlTasks : ACommandCollection
    {
        private const string CLIENT_BALANCE_TAИLE_NAME = "ClientBalance";
        private DataBaseAdapter db;
        private readonly string fileName = "SqlTasks.sqlite";
        private readonly string ORDERS = "Orders";

        public SqlTasks()
        {
            commands.Add(new SimpleCommand("quit exit q", () => IsWork = false));
            commands.Add(new SimpleCommand("create", CreateTables));
            commands.Add(new SimpleCommand("newG", InsertGroup));
            commands.Add(new SimpleCommand("newA", InsertAnalyses));
            commands.Add(new SimpleCommand("newO", InsertOrdes));
            commands.Add(new SimpleCommand("allA", SelectAllAnalyses));
            commands.Add(new SimpleCommand("allG", SelectAllGroup));
            commands.Add(new SimpleCommand("allO", SelectAllOrdes));
            commands.Add(new SimpleCommand("Task1", RunTask1));
            commands.Add(new SimpleCommand("Task2", RunTask2));
            commands.Add(new SimpleCommand("Task3", RunTask3));
            commands.Add(new SimpleCommand("FillO", FillOrder));
            commands.Add(new SimpleCommand("FillB", FillBalance));
            //commands.Add(new SimpleCommand<string>("table", CreateTableTest));

            db = new DataBaseAdapter(fileName);
            if (!db.Exists)
                db.Create();
        }

        private void FillBalance()
        {
            var queryCreate = db.CreateTable();
            queryCreate.Name = CLIENT_BALANCE_TAИLE_NAME;
            queryCreate.IfNotExists = true;
            queryCreate.AddColumn("id INTEGER");
            queryCreate.AddColumn("name TEXT");
            queryCreate.AddColumn("balance_date DATETIME");
            queryCreate.AddColumn("balance_value DOUBLE");

            queryCreate.ExecuteNonQuery();

            db.ExecuteNonQuery($"DELETE FROM {CLIENT_BALANCE_TAИLE_NAME};");

            Random random = new Random();
            int countRec = 0;
            for (int year = 2015; year < 2022; year++)
            {
                Line($"{year}");
                for (int month = 1; month < 13; month++)
                {
                    for (int day = 1; day < 32; day++)
                    {
                        try
                        {
                            DateTime date = new DateTime(year, month, day);
                            if (date > DateTime.Now)
                                return;

                            int count = random.Next(2, 5);
                            InsertIntoQuery q = db.InsertInto();
                            q.Table = CLIENT_BALANCE_TAИLE_NAME;
                            q.SetColumns("id", "name", "balance_date", "balance_value");

                            for (int i = 0; i < count; i++)
                            {
                                if (i != 0)
                                    q.NextLine();

                                int id = random.Next(1, 9);
                                float value = (float)(random.NextDouble() - 0.5);

                                q.Set("id", id);
                                q.Set("name", "Булат");
                                q.Set("balance_date", date);
                                q.Set("balance_value", value);

                                countRec++;

                                bool doublecate = random.Next(3) == 1;

                                if (doublecate)
                                {
                                    q.NextLine();
                                    q.Set("id", id);
                                    q.Set("name", "Булат");
                                    q.Set("balance_date", date);
                                    q.Set("balance_value", value);
                                }
                            }
                            q.ExecuteNonQuery();
                            Text($"{countRec}\r");

                        }
                        catch
                        {

                        }
                    }
                    Line($"{month}: {countRec}");
                }
            }
        }

        private void RunTask3()
        {
            var table = db.Execute(@"
DELETE FROM ClientBalance
WHERE id IN
(
    SELECT id 
    FROM
    (
        SELECT 
            id,
            ROW_NUMBER() OVER 
            (
                PARTITION BY id, name, balance_date, balance_value ORDER BY id
            ) AS rn
        FROM ClientBalance
    )
    WHERE rn > 1
)
");
            WriteTable(table);
        }

        private void FillOrder()
        {
            //db.ExecuteNonQuery($"DELETE FROM IF EXIST {ORDERS};");
            Random random = new Random();
            int countRec = 0;
            Stopwatch t = new Stopwatch();
            t.Start();
            for (int year = 2015; year < 2022; year++)
            {
                Line($"{year}");
                for (int month = 1; month < 13; month++)
                {
                    for (int day = 1; day < 32; day++)
                    {
                        try
                        {
                            DateTime date = new DateTime(year, month, day);
                            if (date > DateTime.Now)
                                return;

                            int count = random.Next(5, 20);
                            StringBuilder query = new StringBuilder("INSERT INTO Orders('time', 'analysis_id') VALUES ");
                            var insert = db.InsertInto();
                            insert.Table = "Orders";
                            insert.SetColumns("time", "analysis_id");


                            for (int i = 0; i < count; i++)
                            {
                                if (i != 0)
                                    insert.NextLine();
                                int аnalysisId = random.Next(1, 9);

                                insert.Set("time", date);
                                insert.Set("analysis_id", аnalysisId);

                                countRec++;
                            }
                            //db.Execute(query.ToString());
                            insert.ExecuteNonQuery();

                            Text($"{countRec}\r");

                        }
                        catch
                        { 
                        
                        }
                    }
                    Line($"{month}: {countRec}");
                }
            }

            t.Stop();
            Space();

            Line($"Выполнялось: {t.Elapsed.TotalSeconds} сек");
        }

        private void RunTask2()
        {
            // https://tproger.ru/articles/5-zadanij-po-sql-s-realnyh-sobesedovanij/
            Line(@"Формулировка: 
Hарастающим итогом рассчитать, как увеличивалось количество проданных тестов 
каждый месяц каждого года с разбивкой по группе.");

            var table = db.Execute(@"
with sales as 
(
    SELECT 
        g.id as groupId,
        strftime('%m', o.time) as month, 
        strftime('%Y', o.time) as year,  
        COUNT(o.analysis_id) as cnt
    FROM Orders o
    JOIN Analyzes a
    ON o.analysis_id == a.id   
    JOIN Groups g
    on a.group_id == g.id
    GROUP BY strftime('%m', o.time), 
        strftime('%Y', o.time),
        g.id 
)
SELECT 
    s.year, s.month, s.groupId,
    SUM(s.cnt) OVER (PARTITION BY s.groupId ORDER BY s.year, s.month) as chack
FROM sales s ORDER BY s.groupID, s.year, s.month");
            WriteTable(table);
        }

        private void RunTask1()
        {
            // https://tproger.ru/articles/5-zadanij-po-sql-s-realnyh-sobesedovanij/
            Line("Формулировка: вывести название и цену для всех анализов, которые продавались 5 февраля 2020 и всю следующую неделю.");
            var table = db.Execute(@"
SELECT a.Name, a.Cost, o.time 
FROM Analyzes a 
JOIN Orders o 
ON o.analysis_id == a.id 
WHERE o.time 
BETWEEN '2020-02-05' AND '2020-02-12'");
            WriteTable(table);
        }

        private void SelectAllOrdes()
        {
            var table = db.Execute("SELECT * FROM Orders");
            WriteTable(table);
        }

        private void InsertOrdes()
        {
            var date = DateTime.Now;
            ConsoleHelper.QueryInt("Введите id анализа: ", out int аnalysisId);

            string query = $@"INSERT INTO Orders('time', 'analysis_id') VALUES ('{date.ToString("yyyy-MM-dd HH:mm:ss")}','{аnalysisId}' );";
            Line($"{query}\nВыполняю...");
            db.ExecuteNonQuery(query);
        }

        private void SelectAllGroup()
        {
            var table = db.Execute("SELECT * FROM Groups");
            WriteTable(table);
        }


        private void SelectAllAnalyses()
        {
            var table = db.Execute("SELECT * FROM Analyzes");
            WriteTable(table);
        }

        private void InsertAnalyses()
        {
            var name = ConsoleHelper.Query("Введите название анализа: ");
            ConsoleHelper.QueryFloat("Введите себестоимость анализа: ", out float cost);
            ConsoleHelper.QueryFloat("Введите розничную цену анализа: ", out float price);
            ConsoleHelper.QueryInt("Введите id группы анализов: ", out int groupId);

            string query = $@"INSERT INTO Analyzes('name', 'cost', 'price', 'group_id') VALUES ('{name}','{cost}','{price}','{groupId}' );";
            Line($"{query}\nВыполняю...");
            db.ExecuteNonQuery(query);
        }

        private void InsertGroup()
        {
            var name = ConsoleHelper.Query("Введите имя новой группы: ");
            var temp = ConsoleHelper.Query("Введите температурный режим: ");

            string query = $@"INSERT INTO Groups ('name', 'temp') VALUES ('{name}','{temp}');";
            Line($"{query}\nВыполняю...");
            db.ExecuteNonQuery(query);
        }

        private void CreateTables()
        {
            string query = $@"CREATE TABLE IF NOT EXISTS Groups
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT, 
    temp TEXT
)";
            Line($"{query}\nВыполняю...");
            db.ExecuteNonQuery(query);

            query = $@"CREATE TABLE IF NOT EXISTS Analyzes
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT, 
    cost FLOAT, 
    price FLOAT, 
    group_id INT,
    CONSTRAINT fk_Groups
            FOREIGN KEY (group_id)
            REFERENCES Groups(id)
)";
            Line($"{query}\nВыполняю...");
            db.ExecuteNonQuery(query);

            query = $@"CREATE TABLE IF NOT EXISTS Orders
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    time DATETIME, 
    analysis_id INT,
    CONSTRAINT fk_Analyzes
            FOREIGN KEY (analysis_id)
            REFERENCES Analyzes(id)
)";
            Line($"{query}\nВыполняю...");
            db.ExecuteNonQuery(query);

            var clientBalance = db.CreateTable();
            clientBalance.Name = CLIENT_BALANCE_TAИLE_NAME;
            clientBalance.IfNotExists = true;
            clientBalance.AddColumn("id INTEGER");
            clientBalance.AddColumn("name TEXT");
            clientBalance.AddColumn("balance_date DATETIME");
            clientBalance.AddColumn("balance_value DOUBLE");
            Line($"{clientBalance.GetQueryString()}\nВыполняю...");
            clientBalance.ExecuteNonQuery();


        }

        private static void WriteTable(System.Data.DataTable table)
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                System.Data.DataRow row = table.Rows[i];

                for (int j = 0; j < row.ItemArray.Length; j++)
                {
                    Console.Write(row[j]);
                    Console.Write('|');
                }
                Console.WriteLine();
            }
        }

        public override void WriteNotify()
        {
            Console.Write("Введите команду sql: ");
        }
    }
}