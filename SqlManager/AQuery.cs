﻿using System.Data;

namespace SqlManager
{
    public abstract class AQuery
    {
        private DataBaseAdapter dataBase;

        public AQuery(DataBaseAdapter dataBase)
        {
            this.dataBase = dataBase;
        }

        public abstract string GetQueryString();

        public DataTable Execute()
        {
            return dataBase.Execute(GetQueryString());
        }

        public void ExecuteNonQuery()
        {
            dataBase.ExecuteNonQuery(GetQueryString());
        }
    }
}