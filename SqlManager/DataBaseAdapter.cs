﻿using System;
using System.Data;
using System.Data.SQLite;

namespace SqlManager
{
    public class DataBaseAdapter
    {
        private string dbFileName;
        private SQLiteConnection m_dbConn;
        private SQLiteCommand m_sqlCmd;

        public DataBaseAdapter(string fileName)
        {
            this.dbFileName = fileName;
            if (Exists)
            {
                Connect();
            }
        }

        public bool Exists => System.IO.File.Exists(dbFileName);

        public void Create()
        {
            if (!Exists)
            {
                SQLiteConnection.CreateFile(dbFileName);
                Connect();
            }
        }

        public void ExecuteNonQuery(string query)
        {
            m_sqlCmd.CommandText = query;
            m_sqlCmd.ExecuteNonQuery();
        }


        public DataTable Execute(string query)
        {
            DataTable dTable = new DataTable();
            if (m_dbConn.State != ConnectionState.Open)
                return dTable;

            SQLiteDataAdapter adapter = new SQLiteDataAdapter(query, m_dbConn);
            adapter.Fill(dTable);

            return dTable;
        }


        private void Connect()
        {
            m_dbConn = new SQLiteConnection("Data Source=" + dbFileName + ";Version=3;");
            m_sqlCmd = new SQLiteCommand();

            m_dbConn.Open();
            m_sqlCmd.Connection = m_dbConn;
        }

        public CreateTableQuery CreateTable()
        {
            return new CreateTableQuery(this);
        }

        public SelectQuery Select()
        {
            return new SelectQuery(this);
        }

        public InsertIntoQuery InsertInto()
        {
            return new InsertIntoQuery(this);
        }

        public DropTableQuery DropTable(string tableName)
        {
            return new DropTableQuery(this, tableName);
        }
    }
}
