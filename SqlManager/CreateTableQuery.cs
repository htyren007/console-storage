﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace SqlManager
{
    // https://www.sqlite.org/lang_createtable.html
    public class CreateTableQuery : AQuery
    {
        private List<string> collumnsText = new List<string>();
        private string primaryKeyString;

        internal CreateTableQuery(DataBaseAdapter dataBase) : base(dataBase)
        {
        }

        public string Name { get; set; }
        public bool IfNotExists { get; set; }

        public override string GetQueryString()
        {
            StringBuilder query = new StringBuilder("CREATE TABLE ");
            if (IfNotExists)
                query.Append("IF NOT EXISTS ");
            query.Append(Name);
            query.Append("(");

            bool first = true;
            if (!string.IsNullOrWhiteSpace(primaryKeyString))
            {
                query.Append(primaryKeyString);
                first = false;
            }

            foreach (var item in collumnsText)
            {
                if (!first)
                    query.Append(", "); 
                else
                    first = false;

                query.Append(item);
            }
            query.Append(")");

            return query.ToString();
        }

        public void AddColumn(string columnName, DbType type, bool notNull = false, bool unique = false)
        {
            StringBuilder builder = NameAndType(columnName, type);

            if (notNull)
                builder.Append(" NOT NULL");

            if (unique)
                builder.Append(" UNIQUE");

            collumnsText.Add(builder.ToString());
        }

        public void AddColumn(string text)
        {
            collumnsText.Add(text);
        }

        public void PrimaryKey(string columnName, DbType type, bool autoIncrement = false, bool unique = false)
        {
            StringBuilder builder = NameAndType(columnName, type);
            builder.Append(" PRIMARY KEY");

            if (autoIncrement)
            {
                builder.Append(" AUTOINCREMENT ");
            }

            if (unique)
            {
                builder.Append(" UNIQUE ");
            }

            primaryKeyString = builder.ToString();
        }

        public void ForeignKey(string columnName, DbType type, string foreignKey, string foreignColumn)
        {
            StringBuilder builder = NameAndType(columnName, type);

            builder.Append(" REFERENCES ");
            builder.Append(foreignKey);
            builder.Append($"({foreignColumn})");

            collumnsText.Add(builder.ToString());
        }

        private static StringBuilder NameAndType(string columnName, DbType type)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(columnName);
            builder.Append(' ');
            builder.Append(GetTypeLebel(type));
            return builder;
        }

        public static string GetTypeLebel(DbType type)
        {
            // https://docs.microsoft.com/ru-ru/dotnet/standard/data/sqlite/types
            switch (type)
            {
                case DbType.Boolean:
                case DbType.SByte:
                case DbType.UInt16:
                case DbType.UInt32:
                case DbType.UInt64:
                case DbType.Int16:
                case DbType.Int32:
                case DbType.Int64:
                case DbType.Byte:
                case DbType.Currency:
                case DbType.VarNumeric:
                case DbType.AnsiStringFixedLength:
                    return "INTEGER";

                case DbType.Single:
                case DbType.Double:
                    return "REAL";

                case DbType.Decimal:
                case DbType.DateTime:
                case DbType.Date:
                case DbType.AnsiString:
                case DbType.Guid:
                case DbType.String:
                case DbType.Time:
                case DbType.Xml:
                case DbType.StringFixedLength:
                case DbType.DateTime2:
                case DbType.DateTimeOffset:
                    return "TEXT";

                case DbType.Binary:
                case DbType.Object:
                    return "BLOB";

                default:
                    throw new ArgumentException($"Не реализованный тип {type}");
            }
        }
    }
}