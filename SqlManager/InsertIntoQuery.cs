﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SqlManager
{
    public class InsertIntoQuery : AQuery
    {
        public InsertIntoQuery(DataBaseAdapter dataBase) : base(dataBase)
        {
            
        }

        public string Table { get; set; }
        public List<string> Columns { get; private set; }
        public List<List<string>> Values { get; private set; } = new List<List<string>>();
        private List<string> CurrentValue { get; set; }

        public override string GetQueryString()
        {
            StringBuilder query = new StringBuilder("INSERT INTO ");
            query.Append(Table);
            query.Append('(');
            bool first = true;
            foreach (var item in Columns)
            {
                if (!first)
                    query.Append(',');
                else
                    first = false;

                query.Append('\'');
                query.Append(item);
                query.Append('\'');
            }
            query.Append(')');
            query.Append(" VALUES");

            first = true;
            foreach (var value in Values)
            {
                if (!first)
                    query.Append(',');
                else
                    first = false;

                query.Append('(');

                var firstItem = true;
                foreach (var item in value)
                {
                    if (!firstItem)
                        query.Append(',');
                    else
                        firstItem = false;

                    query.Append(item);
                }

                query.Append(')');
            }

            return query.ToString();
        }

        public void SetStringEscape(string column, string value)
        {
            throw new NotImplementedException();
        }

        public void Set(string column, bool value)
        {
            SetValue(column, $"'{value}'");
        }

        public void SetColumns(params string[] columns)
        {
            Columns = new List<string>(columns);
            Values.Clear();
            NextLine();
        }

        public void SetNull(string column)
        {
            SetValue(column, $"NULL");
        }

        public void Set(string column, int value)
        {
            SetValue(column, $"'{value}'");
        }

        public void Set(string column, string value)
        {
            SetValue(column, $"\"{value}\"");
        }

        public void Set(string column, DateTime value)
        {
            SetValue(column, $"'{value.ToString("yyyy-MM-dd HH:mm:ss")}'");
        }

        public void Set(string column, float value)
        {
            SetValue(column, $"'{value}'");
        }

        public void NextLine()
        {
            var newValue = new List<string>();
            for (int i = 0; i < Columns.Count; i++)
            {
                newValue.Add("");
            }

            Values.Add(newValue);
            CurrentValue = newValue;
        }

        private void SetValue(string column, string value)
        {
            var index = Columns.IndexOf(column);
            if (index >= 0)
            {
                CurrentValue[index] = value;
            }
        }
    }
}