﻿using System;
using System.Data;
using System.Text;

namespace SqlManager
{
    public class SelectQuery : AQuery
    {
        private string tableName;
        private string expression;
        private int limit;

        public SelectQuery(DataBaseAdapter dataBase) : base(dataBase)
        {
        }

        public override string GetQueryString()
        {
            StringBuilder query = new StringBuilder("SELECT ");
            query.Append("* FROM ");
            query.Append(tableName);
            query.Append(" WHERE ");
            query.Append(expression);
            if (limit > 0)
            {
                query.Append(" LIMIT ");
                query.Append(limit.ToString());
            }

            return query.ToString();
        }

        public SelectQuery From(string tableName)
        {
            this.tableName = tableName;
            return this;
        }

        public SelectQuery Limit(int count)
        {
            this.limit = count;
            return this;
        }

        public SelectQuery Where(string expression)
        {
            this.expression = expression;
            return this;
        }

        public SelectResult ExecuteAndGetResult()
        {
            DataTable answer = Execute();
            return new SelectResult(answer);
        }
    }
}