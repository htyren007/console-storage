﻿using System.Text;

namespace SqlManager
{
    public class DropTableQuery : AQuery
    {
        // https://www.sqlite.org/lang_droptable.html
        private string tableName;

        public DropTableQuery(DataBaseAdapter adapter, string tableName): base(adapter)
        {
            this.tableName = tableName;
        }

        public bool IfExists { get; set; }

        public override string GetQueryString()
        {
            StringBuilder builder = new StringBuilder("DROP TABLE ");
            if (IfExists)
                builder.Append("IF EXISTS ");

            builder.Append(tableName);

            return builder.ToString();
        }
    }
}