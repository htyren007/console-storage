﻿using ConsoleStorage.Command;
using ConsoleStorage.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ConsoleStorageTests
{
    [TestClass]
    public class ACommandCollection_Tests
    {        
        [TestMethod]
        public void FindCommand_1()
        {
            var context = new CommandContext();
            context.Input = "test word2";
            context.CommandName = "test";
            var commands = new TestCommands();

            var command = commands.FindCommand(context);

            Assert.IsInstanceOfType(command, typeof(SimpleCommand));
            var simple = command as SimpleCommand;

            Assert.AreEqual("test", simple.CommandName);

        }

        [TestMethod]
        public void ForEach_1()
        {
            
            var commands = new TestCommands();

            commands.ForEach<SimpleCommand>((com) => { 
                Assert.IsInstanceOfType(com, typeof(SimpleCommand));
                
            });
        }

        [TestMethod]
        public void ForEach_2()
        {

            var commands = new TestCommands();

            commands.ForEach<IntegerCommand>((com) => {
                Assert.IsInstanceOfType(com, typeof(IntegerCommand));
                Assert.AreEqual("int", com.CommandName);
            });
        }

        public class TestCommands : ACommandCollection
        {
            public bool Execute1 { get; private set; }
            public bool Execute2 { get; private set; }
            public IConsoleCommand Command1 { get; private set; }
            public IConsoleCommand Command2 { get; private set; }

            public TestCommands()
            {
                Command1 = new SimpleCommand("test", () => Execute1 = true);
                Command2 = new StringCommand("str", (s) => Execute2 = true);

                commands.Add(Command1);
                commands.Add(Command2);
                commands.Add(new SimpleCommand("test2", () => Execute2 = true));
                commands.Add(new IntegerCommand("int", (i) => Execute2 = true));
                commands.Add(new BoolCommand("bool", (b) => Execute2 = true));
                commands.Add(new ListCommand(this));
                commands.Add(new HelpCommand(this));
                commands.Add(new SimpleCommand<int>("simp_int", (i)=> Execute2 = true, ConvertHelper.StrToInt));
            }
        }
    }
}
