using ConsoleStorage.Command;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace ConsoleStorageTests
{
    [TestClass]
    public class Executor_Tests
    {
        private static bool Executing;

        [TestMethod]
        public void TryParseCommand_1()
        {
            var context = new CommandContext();
            context.Input = "test param1 param2";

            Executor.TryParseCommand(context);

            Assert.IsNotNull(context.Words);
            Assert.AreEqual("test", context.CommandName);
            Assert.AreEqual(3, context.Words.Length);
            Assert.AreEqual("test", context.Words[0]);
            Assert.AreEqual("param1", context.Words[1]);
            Assert.AreEqual("param2", context.Words[2]);

            Assert.AreEqual(2, context.Params.Length);
            Assert.AreEqual("param1", context.Params[0]);
            Assert.AreEqual("param2", context.Params[1]);
        }

        [TestMethod]
        public void RunCommand_1()
        {
            var exe = new Executor();

            Mock<ICommandCollection> commands = CreateMock();
            exe.SetCommands(commands.Object);

            var context = new CommandContext();
            context.Input = "test param1 param2";

            exe.RunCommand(context);

            Assert.IsTrue(Executing);
        }

        private Mock<ICommandCollection> CreateMock()
        {
            var commands = new Mock<ICommandCollection>();
            var command = new Mock<IConsoleCommand>();
            command.Setup(q => q.Run(It.IsAny<CommandContext>())).Callback<CommandContext>(context =>
            {
                Executing = true;
            });
            commands.Setup(x => x.FindCommand(It.IsAny<CommandContext>())).Returns(command.Object);
            return commands;
        }
    }

}
