﻿using ConsoleStorage.Command;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ConsoleStorageTests
{
    [TestClass]
    public class AConsoleCommand_Tests
    {
        [TestMethod]
        public void NeedThis_1()
        {
            var context = new CommandContext();
            context.Input = "test param1 param2";
            context.CommandName = "test";


            var command = new TestCommand();

            command.CommandName = "test";
            var result = command.NeedThis(context);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void NeedThis_2()
        {
            var context = new CommandContext();
            context.Input = "test2 param1 param2";
            context.CommandName = "test2";


            var command = new TestCommand();

            command.CommandName = "test";
            var result = command.NeedThis(context);

            Assert.IsFalse(result);
        }


        public class TestCommand : AConsoleCommand
        {
            public override void Run(CommandContext context)
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
