﻿using System;
using System.Linq;
using System.Reflection;

namespace ConsoleStorage.ConsoleMenu
{
    public class AssemblyHelper
    {
        public static MethodInfo[] GetMethodsWithAttribute(Type type, Type attribute)
        {
            var methods = type.GetMethods()
                .Where(m => m.GetCustomAttributes(attribute, false).Length > 0)
                .ToArray();
            return methods;
        }

        public static PropertyInfo[] GetPropertiesWithAttribute(Type type, Type attribute)
        {
            var properties = type.GetProperties()
                .Where(m => m.GetCustomAttributes(attribute, false).Length > 0)
                .ToArray();
            return properties;
        }
    }
}