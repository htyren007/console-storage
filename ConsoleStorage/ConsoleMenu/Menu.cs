﻿using ConsoleStorage.Slides;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleStorage.ConsoleMenu
{

    /// <summary>
    /// Меню с выбором элементов
    /// Изменить заготовок 
    /// <code>[SlideTitleAttribute("Текст")]</code>
    /// Изменить заготовок 
    /// <code>[SlideTitleAttribute("Текст")]</code>
    /// </summary>
    /// <typeparam name="TController"></typeparam>
    public class Menu<TController> where TController : new()
    {
        private TController contoller;
        private SlideButtonModel[] models;
        private SlideTitleAttribute titleAttribute;

        public Menu()
        {
            contoller = new TController();
            var type = typeof(TController);
            MethodInfo[] methods = AssemblyHelper.GetMethodsWithAttribute(type, typeof(SlideButtonAttribute));
            models = methods.Select(x => new SlideButtonModel(x)).ToArray();
            titleAttribute = type.GetCustomAttribute<SlideTitleAttribute>();
        }

        public void Run()
        {
            Screen screen = new Screen();
            MenuSlide slide = new MenuSlide();
            screen.Slide = slide;
            if (titleAttribute?.Title != null)
                slide.Header.Text = titleAttribute.Title;
            slide.Header.Left = 10;
            slide.Header.Top = 4;
            var hint = new ColorLabel() {Top=Console.WindowHeight-1 };
            slide.AddLabel(hint);

            foreach (var model in models)
            {
                var item = new MenuItem();
                item.Title = model.GetTitle();
                item.Action = () => model.Invoke(contoller, slide);
                item.SelectedChenge += (i) => hint.Text = model.GetHint();
                slide.AddItem(item);
            }


            screen.Run();
        }
    }
}
