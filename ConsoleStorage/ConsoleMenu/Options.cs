﻿using ConsoleStorage.Slides;
using ConsoleStorage.Slides.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ConsoleStorage.ConsoleMenu
{
    public class Options<TController> where TController : new()
    {
        private TController contoller;
        private SlideButtonModel[] buttonModels;
        private SlideTitleAttribute titleAttribute;
        private SlidePropertyModel[] propertyModels;
        private int MarginLeft = 2;
        private MethodInfo[] setup;
        private SlideLabelModel[] labelModels;

        public Options()
        {
            contoller = new TController();
            var type = typeof(TController);
            titleAttribute = type.GetCustomAttribute<SlideTitleAttribute>();

            PropertyInfo[] propertyLabels = AssemblyHelper.GetPropertiesWithAttribute(type, typeof(SlideLabelAttribute));
            labelModels = propertyLabels.Select(x => new SlideLabelModel(x)).ToArray();

            PropertyInfo[] propertyInput = AssemblyHelper.GetPropertiesWithAttribute(type, typeof(SlidePropertyAttribute));
            propertyModels = propertyInput.Select(x => new SlidePropertyModel(x)).ToArray();

            MethodInfo[] methodButtons = AssemblyHelper.GetMethodsWithAttribute(type, typeof(SlideButtonAttribute));
            buttonModels = methodButtons.Select(x => new SlideButtonModel(x)).ToArray();

            MethodInfo[] setupMethods = AssemblyHelper.GetMethodsWithAttribute(type, typeof(SlideSetupAttribute));
            setup = setupMethods.Where(m =>
            {
                var p = m.GetParameters();
                return (p.Length == 1 && p[0].ParameterType == typeof(CustomForm));
            }).ToArray();
        }

        public TController Controller => contoller;

        public void Run()
        {
            Screen screen = new Screen();
            CustomForm slide = new CustomForm();

            screen.Slide = slide;
            int line = 2;
            int colum = 0;
            int columWidth = Console.WindowWidth / 2 - MarginLeft;

            CreateTitle(slide);

            foreach (var method in setup)
            {
                method.Invoke(contoller, new object[] { slide });
            }

            foreach (var item in labelModels)
            {
                InteractionElement field = null;
                var label = new TextField();
                label.Left = item.IsCustomPosition ? item.Left : MarginLeft;
                label.Top = item.IsCustomPosition ? item.Top : line++;
                label.Content = item.GetValue(contoller).ToString();
                label.BeforeRender += () => label.Content = item.GetValue(contoller).ToString();
                slide.Add(label);
            }

            foreach (var item in propertyModels)
            {
                InteractionElement field = null;
                if (item.Info.PropertyType == typeof(bool))
                {
                    var checkBox = new CheckBox()
                    {
                        Content = item.GetText()
                    };
                    checkBox.ValueChange += (v) => item.SetValue(contoller, v);
                    checkBox.BeforeRender += () => checkBox.IsChecked = (bool)item.GetValue(contoller);
                    field = checkBox;
                }

                if (item.Info.PropertyType == typeof(string))
                {
                    //int left = item.IsCustomPosition ? item.Left : MarginLeft;
                    //// Todo: пока надпись будет на одну линию выше
                    //int top = item.IsCustomPosition ? item.Top - 1 : line++;

                    //var label = new Label(item.GetText(), left, top);
                    //slide.Add(label);
                    var input = new InputField();
                    input.ValueChange += (v) => item.SetValue(contoller, v);
                    input.BeforeRender += () => input.Content = (string)item.GetValue(contoller);
                    field = input;
                }

                if (field != null)
                {
                    if (item.IsCustomPosition)
                    {
                        field.Left = item.Left;
                        field.Top = item.Top;
                    }
                    else
                    {
                        field.Top = line++;
                        field.Left = MarginLeft;
                    }


                    slide.Add(field);
                }
            }

            line += 4;
            foreach (var item in buttonModels)
            {
                Button button = new Button();
                button.Content = item.GetTitle();
                button.Action = () => item.Invoke(contoller, slide);

                if (item.IsCustomPosition)
                {
                    button.Left = item.Left;
                    button.Top = item.Top;
                }
                else
                {
                    button.Left = MarginLeft;
                    button.Top = line++;
                }

                slide.Add(button);
            }

            screen.Run();
        }

        private void CreateTitle(CustomForm slide)
        {
            if (titleAttribute != null)
            {
                TextField header = new TextField() {Content = titleAttribute.Title};
                header.BackgroundNormal = ConsoleColor.DarkBlue;
                header.ForegroundNormal = ConsoleColor.White;
                header.Left = titleAttribute.Left == 0 ? (Console.WindowWidth - titleAttribute.Title.Length) / 2 : titleAttribute.Left;
                header.Top = titleAttribute.Top;

                slide.Add(header);
            }
        }
    }
}
