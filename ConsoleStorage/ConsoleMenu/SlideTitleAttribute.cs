﻿using System;

namespace ConsoleStorage.ConsoleMenu
{
    public class SlideTitleAttribute : Attribute
    {
        public SlideTitleAttribute(string title)
        {
            Title = title;
        }

        public SlideTitleAttribute(string title, int left, int top) : this(title)
        {
            Left = left;
            Top = top;
        }

        public string Title { get; }
        public int Left { get; } = 0;
        public int Top { get; } = 0;
    }
}
