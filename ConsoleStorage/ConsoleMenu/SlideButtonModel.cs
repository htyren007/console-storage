﻿using ConsoleStorage.Slides;
using System;
using System.Reflection;

namespace ConsoleStorage.ConsoleMenu
{
    public class SlideLabelModel 
    {
        public PropertyInfo Info;
        private SlideLabelAttribute attribute;

        public SlideLabelModel(PropertyInfo info)
        {
            this.Info = info;
            attribute = info.GetCustomAttribute<SlideLabelAttribute>();
            IsCustomPosition = attribute.Left != 0 || attribute.Top != 0;
        }

        public bool IsCustomPosition { get; }
        public int Left => attribute.Left;
        public int Top => attribute.Top;

        internal object GetValue(object contoller)
        {
            return Info.GetValue(contoller);
        }
    }
    public class SlidePropertyModel
    {
        public PropertyInfo Info;
        private SlidePropertyAttribute attribute;

        public SlidePropertyModel(PropertyInfo info)
        {
            this.Info = info;
            attribute = info.GetCustomAttribute<SlidePropertyAttribute>();
            IsCustomPosition = attribute.Left != 0 || attribute.Top != 0;
        }

        public bool IsCustomPosition { get; }
        public int Left => attribute.Left;
        public int Top => attribute.Top;

        internal string GetText()
        {
            if (attribute.DispayName != null)
                return attribute.DispayName;
            return Info.Name;
        }

        internal object GetValue(object contoller)
        {
            return Info.GetValue(contoller);
        }

        internal void SetValue(object contoller, object newValue)
        {
            Info.SetValue(contoller, newValue);
        }
    }
    public class SlideButtonModel : BaseModel
    {
        private MethodInfo info;
        private SlideButtonAttribute menuItem;
        public bool IsCustomPosition { get; }
        public int Left => menuItem.Left;
        public int Top => menuItem.Top;

        public SlideButtonModel(MethodInfo info)
        {
            this.info = info;
            menuItem = info.GetCustomAttribute<SlideButtonAttribute>();
            IsCustomPosition = menuItem.Left != 0 || menuItem.Top != 0;
        }

        public string GetTitle()
        {
            if (menuItem.DispayName != null)
                return menuItem.DispayName;
            return info.Name;
        }

        public void Invoke(object contoller, ASlide slide)
        {
            Console.ResetColor();
            Console.Clear();
            var countParams = info.GetParameters().Length;
            if (countParams == 1)
                info.Invoke(contoller, new object[] { slide });
            else if (countParams == 0)
                info.Invoke(contoller, new object[] { });
            Console.ResetColor();
            Console.Clear();
        }

        public string GetHint()
        {
            return menuItem.Hint ?? "";
        }
    }
}