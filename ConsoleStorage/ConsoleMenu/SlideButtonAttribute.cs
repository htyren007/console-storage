﻿using System;

namespace ConsoleStorage.ConsoleMenu
{
    public class SlideButtonAttribute : Attribute
    {
        public SlideButtonAttribute(string dispayName=null, string hint=null, int left=0, int top=0) 
        {
            DispayName = dispayName;
            Hint = hint;
            Left = left;
            Top = top;
        }

        public string DispayName { get; }
        public string Hint { get;  }
        public int Left { get;  }
        public int Top { get;  }
    }
}
