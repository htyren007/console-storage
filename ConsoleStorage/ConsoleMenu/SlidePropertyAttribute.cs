﻿using System;

namespace ConsoleStorage.ConsoleMenu
{
    public class SlidePropertyAttribute : Attribute
    {
        

        public SlidePropertyAttribute(string dispayName = null, int left = 0, int top = 0)
        {
            DispayName = dispayName;
            Left = left;
            Top = top;
        }

        public int Left { get; }
        public int Top { get; }
        public string DispayName { get ;  }
    }
}
