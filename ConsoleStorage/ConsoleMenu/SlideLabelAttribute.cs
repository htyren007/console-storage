﻿using System;

namespace ConsoleStorage.ConsoleMenu
{
    public class SlideLabelAttribute : Attribute
    {
        public SlideLabelAttribute(int left = 0, int top = 0)
        {
            Left = left;
            Top = top;
        }

        public int Left { get; }
        public int Top { get; }
    }
}
