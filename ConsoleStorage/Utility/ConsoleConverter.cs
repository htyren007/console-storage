﻿using System;

namespace ConsoleStorage.Utility
{
    public class ConsoleConverter
    {
        public static bool Convert<TOut>(string input, out TOut output)
        {
            var type = typeof(TOut);

            try
            {
                if (type == typeof(string) && input is TOut str)
                {
                    output = str;
                    return true;
                }

                else if (type == typeof(int))
                {
                    int num = System.Convert.ToInt32(input);
                    if (num is TOut numOut)
                    {
                        output = numOut;
                        return true;
                    }
                }

                else if (type == typeof(float))
                {
                    var num = System.Convert.ToSingle(input);
                    if (num is TOut numOut)
                    {
                        output = numOut;
                        return true;
                    }
                }
            }
            catch 
            {
            }
            output = default;
            return false;
        }
    }
}