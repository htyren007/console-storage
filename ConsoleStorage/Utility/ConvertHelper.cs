﻿namespace ConsoleStorage.Utility
{
    public static class ConvertHelper
    {

        public static int ToInt(string arg, int @default = 0)
        {
            if (int.TryParse(arg, out int value))
                return value;
            return @default;
        }

        public static int StrToInt(string arg)
        {
            if (int.TryParse(arg, out int value))
                return value;
            return default;
        }

        public static bool Convert<TOut>(string input, out TOut output)
        {
            var type = typeof(TOut);

            try
            {
                if (type == typeof(string) && input is TOut str)
                {
                    output = str;
                    return true;
                }

                else if (type == typeof(int))
                {
                    int num = System.Convert.ToInt32(input);
                    if (num is TOut numOut)
                    {
                        output = numOut;
                        return true;
                    }
                }

                else if (type == typeof(float))
                {
                    var num = System.Convert.ToSingle(input);
                    if (num is TOut numOut)
                    {
                        output = numOut;
                        return true;
                    }
                }
            }
            catch
            {
            }
            output = default;
            return false;
        }

        public static bool StrToBool(string arg)
        {
            if (bool.TryParse(arg, out bool value))
                return value;
            var str = arg.ToLower();
            if (str == "да" 
                || str == "д"
                || str == "yes"
                || str == "y")
                return true;

            return false;
        }
    }
}
