﻿using System;

namespace ConsoleStorage.Slides.Forms
{
    public class CustomForm : Form
    {
        public event Action BeforeRender;
        public event Action AfterRender;

        public CustomForm()
        {
        }

        public override void BeforeRendering()
        {
            BeforeRender?.Invoke();
            base.BeforeRendering();
        }

        public override void AfterRendering()
        {
            base.AfterRendering();
            AfterRender?.Invoke();
        }

        public void Add(InteractionElement field)
        {
            AddElement(field);
        }

        public void Add(ILabel label)
        {
            AddLabel(label);
        }
    }
}