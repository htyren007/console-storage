﻿using System;

namespace ConsoleStorage.Slides.Forms
{
    public class Button : InteractionElement
    {
        public Button()
        {
            BackgroundNormal = ConsoleColor.Blue;
            ForegroundNormal = ConsoleColor.White;
            BackgroundSelect = ConsoleColor.DarkBlue;
            ForegroundSelect = ConsoleColor.Yellow;
        }

        public Action Action { get; set; }
        public int Width { get; set; }

        public override void ActionHandler(ConsoleKeyInfo key)
        {
            if (key.Key == ConsoleKey.Enter || key.Key == ConsoleKey.Spacebar)
            {
                Action?.Invoke();
            }
        }

        public override void Write()
        {
            string text = "";
            var contentLength = Content.Length;
            if (Width - contentLength <= 0)
                text = Content.Substring(0, Width);
            else if(Width - contentLength > 0) 
            {
                var q = Width - contentLength;
                var field = new string(' ', q / 2);
                text = $"{field}{Content}{field}";
            }
            var oldContent = Content;
            Content = text;
            base.Write();
            Content = oldContent;
        }
    }
}