﻿using System;

namespace ConsoleStorage.Slides.Forms
{
    public class CheckBox : InteractionElement
    {
        private bool isChecked;

        public CheckBox()
        {
            BackgroundNormal = ConsoleColor.DarkGray;
            ForegroundNormal = ConsoleColor.White;
            BackgroundSelect = ConsoleColor.DarkBlue;
            ForegroundSelect = ConsoleColor.White;
        }

        public event Action<bool> ValueChange;

        public bool IsChecked
        {
            get => isChecked;
            set
            {
                if (isChecked != value)
                {
                    isChecked = value;
                    ValueChange?.Invoke(value);
                }
            }
        }

        public int Width { get; set; }

        public override void ActionHandler(ConsoleKeyInfo key)
        {
            if (key.Key == ConsoleKey.Spacebar)
            {
                IsChecked = !IsChecked;
            }
        }

        protected override void WriteLabel()
        {
            if (IsChecked)
                Text("[x] ");
            else
                Text("[ ] ");
            base.WriteLabel();
            //if (Label == null)
            //    Text(new string(' ', Width - 4));
            //else
            //    Text(new string(' ', Width - Label.Length - 4));
        }

    }
}