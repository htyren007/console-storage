﻿using ConsoleStorage.Command;
using System;

namespace ConsoleStorage.Slides.Forms
{
    public abstract class InteractionElement : AConsoleWriter, ILabel
    {
        private bool isSelected;
        private bool isSelectable = true;

        /// <summary>
        /// Элемент выбран
        /// </summary>
        public event Action<bool> SelectedChange;
        public event Action BeforeRender;
        public event Action AfterRender;

        public string Content { get; set; }

        public int Left { get; set; }

        public int Top { get; set; }

        /// <summary> Цвет фона выделенного элемента</summary>
        public ConsoleColor BackgroundSelect { get; set; } = ConsoleColor.DarkBlue;

        /// <summary> Цвет текста выделенного элемента</summary>
        public ConsoleColor ForegroundSelect { get; set; } = ConsoleColor.White;

        /// <summary> Цвет фона элемента</summary>
        public ConsoleColor BackgroundNormal { get; set; } = ConsoleColor.Black;

        /// <summary> Цвет текста элемента</summary>
        public ConsoleColor ForegroundNormal { get; set; } = ConsoleColor.Gray;

        public bool IsSelectable
        {
            get => isSelectable;
            set
            {
                if (isSelectable != value)
                {
                    isSelectable = value;
                }
            }
        }
        public bool IsSelected
        {
            get => isSelected;
            set
            {
                if (isSelected != value)
                {
                    isSelected = value;
                    SelectedChange?.Invoke(value);
                }
                
            }
        }

        /// <summary>Перед отрисовкой всех элементов</summary>
        public virtual void BeforeRendering()
        {
            BeforeRender?.Invoke();
        }

        /// <summary>После отрисовки всех элементов</summary>
        public virtual void AfterRendering()
        {
            AfterRender?.Invoke();
        }

        public virtual void Write()
        {
            if (IsSelected)
                SetColors(ForegroundSelect, BackgroundSelect);
            else
                SetColors(ForegroundNormal, BackgroundNormal);
            SetCursorPosition(Left, Top);
            WriteLabel();
        }

        protected virtual void WriteLabel() => Text(Content);

        public abstract void ActionHandler(ConsoleKeyInfo key);

    }
}