﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleStorage.Slides
{
    namespace Forms
    {
        public class Form : ASlide
        {
            private List<InteractionElement> elements = new List<InteractionElement>();
            private List<ILabel> labels = new List<ILabel>();
            private InteractionElement selected;

            public override IEnumerable<ILabel> Labels
            {
                get
                {
                    return elements.Concat(labels);
                }
            }

            public override void AfterRendering()
            {
                base.AfterRendering();

                foreach (var element in elements)
                {
                    element.AfterRendering();
                }
            }

            public override void BeforeRendering()
            {
                base.BeforeRendering();

                foreach (var element in elements)
                {
                    element.BeforeRendering();
                }
            }

            public override void OnPressKey(ConsoleKeyInfo info)
            {
                switch (info.Key)
                {
                    case ConsoleKey.UpArrow:
                        UpHandler(); break;
                    case ConsoleKey.LeftArrow:
                        LeftHandler(); break;
                    case ConsoleKey.DownArrow:
                        DownHandler(); break;
                    case ConsoleKey.RightArrow:
                        RightHandler(); break;
                    case ConsoleKey.Escape: EscapeHandler(); break;

                    default:
                        UpdateField(info);
                        break;
                }
            }

            protected virtual void EscapeHandler()
            {
                NextSlide = null;
            }


            protected void AddElement(InteractionElement element)
            {
                elements.Add(element);
            }

            protected void AddLabel(ILabel label)
            {
                labels.Add(label);
            }

            private void UpdateField(ConsoleKeyInfo info)
            {
                if (selected != null)
                    selected.ActionHandler(info);
            }

            public static float Dictantion(int x1, int y1, int x2, int y2)
            {

                return (float)Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
            }

            private void RightHandler()
            {
                if (selected == null)
                {
                    UpdateSelected(elements.Last());
                    return;
                }
                var element = GetNearest(elements.Where(x => x.Left > selected.Left));
                if (element != null)
                    UpdateSelected(element);
            }

            private void LeftHandler()
            {
                if (selected == null)
                {
                    UpdateSelected(elements.Last());
                    return;
                }
                var element = GetNearest(elements.Where(x => x.Left < selected.Left));
                if (element != null)
                    UpdateSelected(element);
            }

            private void UpHandler()
            {
                if (selected == null)
                {
                    UpdateSelected(elements.Last());
                    return;
                }
                var element = GetNearest(elements.Where(x => x.Top < selected.Top));
                if (element != null)
                    UpdateSelected(element);
                // OffsetSelected(-1);
            }

            private void DownHandler()
            {
                if (selected == null)
                {
                    UpdateSelected(elements.First());
                    return;
                }
                var element = GetNearest(elements.Where(x => x.Top > selected.Top));
                if (element != null)
                    UpdateSelected(element);

                //OffsetSelected(1);
            }

            /// <summary>
            /// Получить ближайшую к selected
            /// </summary>
            /// <param name="element"></param>
            /// <param name="_elements"></param>
            /// <returns></returns>
            private InteractionElement GetNearest(IEnumerable<InteractionElement> _elements)
            {
                double dist = double.MaxValue;
                InteractionElement element = null;
                foreach (var item in _elements)
                {
                    if (!item.IsSelectable) 
                        continue;
                    var currentDist = Dictantion(item.Left, item.Top, selected.Left, selected.Top);
                    if (currentDist < dist)
                    {
                        dist = currentDist;
                        element = item;
                    }
                }

                return element;
            }

            private void OffsetSelected(int offset)
            {
                int index = elements.IndexOf(selected);
                index += offset;
                if (index >= 0 && index < elements.Count)
                {
                    UpdateSelected(elements[index]);
                }
            }

            private void UpdateSelected(InteractionElement element)
            {
                if (selected != null)
                    selected.IsSelected = false;
                selected = element;
                selected.IsSelected = true;
            }

            public void RemoveElement(InteractionElement element)
            {
                elements.Remove(element);
            }
        }
    }
}