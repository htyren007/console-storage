﻿using System;

namespace ConsoleStorage.Slides.Forms
{
    public class InputField : InteractionElement
    {
        public InputField()
        {
            BackgroundNormal = ConsoleColor.DarkGray;
            ForegroundNormal = ConsoleColor.White;
            BackgroundSelect = ConsoleColor.DarkBlue;
            ForegroundSelect = ConsoleColor.White;
        }

        public event Action<string> ValueChange;

        public int Width { get; set; } = 30;

        public override void ActionHandler(ConsoleKeyInfo key)
        {
            if (char.IsLetter(key.KeyChar))
            {
                Content = Content + key.KeyChar;
            }
            else if (key.Key == ConsoleKey.Backspace)
            {
                Content = Content.Substring(0, Content.Length - 1);
            }
            else if (key.Key == ConsoleKey.Spacebar)
            {
                Content = Content = Content + " ";
            }
            ValueChange?.Invoke(Content);
        }

        public override void AfterRendering()
        {
            base.AfterRendering();
            if (IsSelected)
            {
                if (Content == null)
                    SetCursorPosition(Left, Top);
                else
                    SetCursorPosition(Left + Content.Length, Top);
            }
        }

        protected override void WriteLabel()
        {
            base.WriteLabel();
            if (Content == null)
                Text(new string(' ', Width));
            else
                Text(new string(' ', Width - Content.Length));
        }

    }
}