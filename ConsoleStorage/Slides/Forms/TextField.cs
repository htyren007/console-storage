﻿using System;

namespace ConsoleStorage.Slides.Forms
{
    public class TextField : InteractionElement
    {
        public TextField()
        {
            IsSelectable = false;
        }

        public int Widht { get; set; }
        public HorizontalTypeAlignment Alignment { get; set; }

        public override void ActionHandler(ConsoleKeyInfo key)
        {
            
        }

        public override void Write()
        {
            int left=0, top=Top;
            switch (Alignment)
            {
                case HorizontalTypeAlignment.Center:
                    left = (Left + Widht - Content.Length) / 2;
                    break;
                case HorizontalTypeAlignment.Right:
                    left = Left + Widht - Content?.Length ?? 0;
                    break;
                case HorizontalTypeAlignment.Left:
                default:
                    left = Left;
                    break;
            }

            SetCursorPosition(left, top);
            SetColors(ForegroundNormal, BackgroundNormal);
            WriteLabel();
        }

    }

    public enum HorizontalTypeAlignment
    {
        Left, Center, Right
    }
}