﻿using System;

namespace ConsoleStorage.Slides
{
    public abstract class AMenuItem : ILabel
    {
        public static ConsoleColor SelectedBackgroundColor = ConsoleColor.Blue;
        public static ConsoleColor SelectedForegroundColor = ConsoleColor.White;
        private bool isSelected;

        public string Title { get; set; }
        public int Left { get; set; } = 0;
        public int Top { get; set; } = 0;
        public bool IsSelected { 
            get => isSelected; 
            set { 
                isSelected = value; 
                SelectedChenge?.Invoke(isSelected); 
            } 
        }

        public void Write()
        {
            if (IsSelected)
            {
                Console.BackgroundColor = SelectedBackgroundColor;
                Console.ForegroundColor = SelectedForegroundColor;
            }

            Console.SetCursorPosition(Left, Top);
            Console.Write(Title);
        }

        public void SetPosition(int left, int top)
        {
            Left = left;
            Top = top;
        }

        public abstract void Execute();

        public event Action<bool> SelectedChenge;

    }
}