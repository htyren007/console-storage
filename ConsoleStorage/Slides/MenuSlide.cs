﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleStorage.Slides
{
    public class MenuSlide : ASlide
    {
        private List<ILabel> labels;
        private List<AMenuItem> items;
        private AMenuItem selected;

        public MenuSlide(): base()
        {
            Header = new ColorLabel()
            {
                Text = "Header",
                Background = ConsoleColor.Yellow,
                Foreground = ConsoleColor.Red,
            };
            labels = new List<ILabel>();
            labels.Add(Header);
            items = new List<AMenuItem>();
        }

        public override IEnumerable<ILabel> Labels { get => labels; }

        public ColorLabel Header { get; set; }

        public AMenuItem Selected => selected; 

        public override void BeforeRendering()
        {
            int index = 1;
            int left = Header.Left;
            int top = Header.Top;
            foreach (var item in items)
            {
                item.SetPosition(left, top + index);
                index++;
            }
        }

        public override void OnPressKey(ConsoleKeyInfo key)
        {
            switch (key.Key)
            {
                case ConsoleKey.UpArrow: UpHandler();break;
                case ConsoleKey.DownArrow: DownHandler(); break;
                case ConsoleKey.Escape: EscapeHandler(); break;

                case ConsoleKey.Enter:
                case ConsoleKey.Spacebar:
                    EnterHandler();
                    break;
            }
        }

        public void AddLabel(ILabel label) => labels.Add(label);

        public void AddItem(AMenuItem item)
        {
            labels.Add(item);
            items.Add(item);
        }

        protected virtual void EnterHandler()
        {
            Selected.Execute();
        }

        protected virtual void EscapeHandler()
        {
            NextSlide = null;
        }

        protected virtual void DownHandler()
        {
            if (selected == null)
            {
                UpdateSelected(items.First());
                return;
            }
            OffsetSelected(1);
        }

        protected virtual void UpHandler()
        {
            if (selected == null)
            {
                UpdateSelected(items.Last());
                return;
            }
            OffsetSelected(-1);
        }

        private void OffsetSelected(int offset)
        {
            int index = items.IndexOf(selected);
            index += offset;
            if (index >= 0 && index < items.Count)
            {
                UpdateSelected(items[index]);
            }
        }

        private void UpdateSelected(AMenuItem item)
        {
            if (selected!= null) 
                selected.IsSelected = false;
            selected = item;
            selected.IsSelected = true;
        }
    }
}