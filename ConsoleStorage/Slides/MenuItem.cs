﻿using System;

namespace ConsoleStorage.Slides
{
    public class MenuItem : AMenuItem
    {
        public Action Action { get; set; }


        public override void Execute()
        {
            Action?.Invoke();
        }
    }
}