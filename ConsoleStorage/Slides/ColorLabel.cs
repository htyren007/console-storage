﻿using System;

namespace ConsoleStorage.Slides
{
    public class ColorLabel : ILabel
    {
        public ConsoleColor Background { get; set; } = ConsoleColor.Black;
        public ConsoleColor Foreground { get; set; } = ConsoleColor.White;
        public string Text { get; set; } = string.Empty;
        public int Left { get; set; } = 0;
        public int Top { get; set; } = 0;

        public virtual void Write()
        {
            Console.BackgroundColor = Background;
            Console.ForegroundColor = Foreground;
            Console.SetCursorPosition(Left, Top);
            Console.Write(Text);
        }
    }
}
