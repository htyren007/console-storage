﻿namespace ConsoleStorage.Slides
{
    public interface ILabel
    {
        void Write();
    }
}
