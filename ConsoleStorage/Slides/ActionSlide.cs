﻿using System;
using System.Collections.Generic;

namespace ConsoleStorage.Slides
{
    public class ActionSlide : ASlide
    {
        private List<ILabel> labels= new List<ILabel>();
        private List<KeyAction> keyActions = new List<KeyAction>();
        private List<Action> preparingActions = new List<Action>();

        public ActionSlide():base()
        {

        }

        public override IEnumerable<ILabel> Labels { get => labels; }
        public override void OnPressKey(ConsoleKeyInfo info)
        {
            foreach (var item in keyActions)
            {
                if (item.Key == info.Key)
                    item.Action();
            }
        }
        public override void BeforeRendering()
        {
            base.BeforeRendering();
            foreach (var item in preparingActions)
            {
                item();
            }
        }

        public void AddAction(ConsoleKey key, Action action) => keyActions.Add(new KeyAction { Key = key, Action = action });
        public void AddPrepareAction(Action action) => preparingActions.Add(action);
        public void AddLabel(ILabel label) => labels.Add(label);

        class KeyAction
        {
            public ConsoleKey Key;
            public Action Action;
        }
    }
}