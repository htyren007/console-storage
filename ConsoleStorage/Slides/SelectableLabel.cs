﻿using System;

namespace ConsoleStorage.Slides
{
    public class SelectableLabel : ColorLabel
    {
        public static ConsoleColor SelectedBackgroundColor = ConsoleColor.Blue;
        public static ConsoleColor SelectedForegroundColor = ConsoleColor.White;


        public bool IsSelected { get; set; }

        public override void Write()
        {
            if (IsSelected)
            {
                Console.BackgroundColor = SelectedBackgroundColor;
                Console.ForegroundColor = SelectedForegroundColor;
            }
            else
            {
                Console.BackgroundColor = Background;
                Console.ForegroundColor = Foreground;
            }
            Console.SetCursorPosition(Left, Top);
            Console.Write(Text);
        }

        public void SetPosition(int left, int top)
        {
            Left = left;
            Top = top;
        }
    }
}
