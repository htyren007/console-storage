﻿using System;
using System.Collections.Generic;

namespace ConsoleStorage.Slides
{
    public class Screen
    {
        public ASlide Slide { get; set; }

        public bool IsWork { get; private set; } = true;
        //public ConsoleColor Background { get; set; } = ConsoleColor.Black;
        //public ConsoleColor Foreground { get; set; } = ConsoleColor.White;

        public void Run()
        {
            ConsoleKeyInfo key;
            do
            {
                Print();
                key = Console.ReadKey();
                Update(key);
            }
            while (IsWork);

            Console.ResetColor();
            Console.Clear();
        }

        private void Print()
        {
            Console.BackgroundColor = Slide.Background;
            Console.Clear();

            if (Slide.Labels!= null)
            {
                Slide.BeforeRendering();
                foreach (var label in Slide.Labels)
                {
                    Console.BackgroundColor = Slide.Background;
                    Console.ForegroundColor = Slide.Foreground;
                    label.Write();
                }
                Console.SetCursorPosition(0, 0);
                Slide.AfterRendering();
            }

        }

        private void Update(ConsoleKeyInfo key)
        {
            Slide.OnPressKey(key);

            if (Slide.NextSlide != null)
                Slide = Slide.NextSlide;
            else
                IsWork = false;
        }
    }
}
