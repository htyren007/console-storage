﻿using System;

namespace ConsoleStorage.Slides
{
    public class Label : ILabel
    {
        private string text;
        private int left;
        private int top;

        public Label(string text, int left, int top)
        {
            this.text = text;
            this.left = left;
            this.top = top;
        }

        public virtual void Write()
        {
            Console.SetCursorPosition(left, top);
            Console.Write(text);
        }
    }
}
