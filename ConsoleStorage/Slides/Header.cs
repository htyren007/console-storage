﻿using System;

namespace ConsoleStorage.Slides
{
    public class Header : Label
    {

        public Header(string text, int left, int top): base(text, left, top)
        {
        }

        public override void Write()
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;

            base.Write();
        }
    }
}