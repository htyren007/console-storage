﻿using System;
using System.Collections.Generic;

namespace ConsoleStorage.Slides
{
    public abstract class ASlide
    {
        protected ASlide()
        {
            NextSlide = this;
            Background = ConsoleColor.Black;
            Foreground = ConsoleColor.White;
        }

        public ConsoleColor Background { get; protected set; }
        public virtual IEnumerable<ILabel> Labels { get; protected set; }
        public ConsoleColor Foreground { get; protected set; }

        /// <summary>
        /// Следующий слад после <see cref="OnPressKey(ConsoleKeyInfo)"/>
        /// </summary>
        public ASlide NextSlide { get; set; }

        /// <summary>
        /// Основная логика и обработка нажатия кнопки
        /// </summary>
        /// <param name="info"></param>
        public abstract void OnPressKey(ConsoleKeyInfo info);

        /// <summary>
        /// Перед отрисовкой
        /// </summary>
        public virtual void BeforeRendering()
        {
        }

        /// <summary>
        /// После отрисовки и перед вводом
        /// </summary>
        public virtual void AfterRendering()
        {
        }
    }
}