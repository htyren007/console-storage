﻿using System;
using System.Collections.Generic;

namespace ConsoleStorage.Data
{
    public static partial class Coder
    {
        internal class Enigma
        {
            List<Rotor> Rotors;
            bool Unicode;
            string Simbols = "";

            public Enigma(string key, string simbols = "")
            {
                Unicode = string.IsNullOrEmpty(simbols);
                Simbols = simbols;
                Rotors = new List<Rotor>();
                int incerment = /*1680 **/ key.Length;
                foreach (char ch in key)
                {
                    Rotor rotor;
                    if (Unicode)
                        rotor = new Rotor(Convert.ToUInt16(ch), (ushort)incerment);
                    else
                    {
                        if (!Simbols.Contains(ch.ToString())) throw new ArgumentException("Символ не представлен в строке!");
                        rotor = new Rotor((ushort)Simbols.IndexOf(ch), (ushort)incerment, (ushort)Simbols.Length);
                    }
                    Rotors.Add(rotor);
                }
            }


            public char Code(char simbol)
            {
                ushort max = (Unicode) ? ushort.MaxValue : (ushort)Simbols.Length;
                ushort n_simbol;
                if (Unicode)
                    n_simbol = Convert.ToUInt16(simbol);
                else
                    n_simbol = (ushort)Simbols.IndexOf(simbol);

                if (n_simbol < 0 || n_simbol > max) throw new ArgumentException("Символ не представлен в строке!");
                //Console.WriteLine("in:" + n_simbol);
                for (int i = 0; i < Rotors.Count; i++)
                {
                    Rotor rotor = Rotors[i];
                    n_simbol = rotor.RotateSimbol(n_simbol);
                    //Console.WriteLine($"{i}: {n_simbol}[{rotor.Point}]");
                }
                //Console.WriteLine("~:" + n_simbol);
                n_simbol = (ushort)(((ushort)~n_simbol) % max);
                //Console.WriteLine("~:" + n_simbol);
                for (int i = Rotors.Count - 1; i >= 0; i--)
                {
                    Rotor rotor = Rotors[i];
                    n_simbol = rotor.UnrotateSimbol(n_simbol);
                    //Console.WriteLine($"{i}: {n_simbol}[{rotor.Point}]");
                    rotor.Rotate();
                }
                //Console.WriteLine("out:" + n_simbol);
                //Console.ReadLine();
                if (Unicode)
                    return Convert.ToChar(n_simbol);
                else
                    return Simbols[n_simbol];
            }


        }

    }

}
