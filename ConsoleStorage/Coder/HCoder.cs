﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleStorage.Data
{
    public static partial class Coder
    {
        /// <summary>
        /// Уникоде
        /// </summary>
        /// <param name="text"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string Code(string text, string key)
        {
            if (string.IsNullOrEmpty(key)) return text;
            Enigma enigma = new Enigma(key);
            StringBuilder result = new StringBuilder();

            foreach (char ch in text)
            {
                result.Append(enigma.Code(ch));
            }

            return result.ToString();
        }

        readonly static string SET_A = ABC + ABC_RUS + NUMERIC + CHARS;
        readonly static string SET_B = ABC + NUMERIC + CHARS;
        const string ABC = @"ABCDEFGHJIKLMNOPQRSTUVWXYZabcdefghjiklmnopqrstuvwxyz";
        const string ABC_RUS = "йцукенгшщзхъфывапролджэячсмитьбюЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮЁё";
        const string NUMERIC = "1234567890";
        const string CHARS = @"~!@#$%^&*(){}[]:;'|\/?><,._-+=`""";

        /// <summary>
        /// Русский и английский чифры знаки
        /// </summary>
        /// <param name="text"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string CodeA(string text, string key)
        {
            if (string.IsNullOrEmpty(key)) return text;
            Enigma enigma = new Enigma(key, SET_A);
            StringBuilder result = new StringBuilder();

            foreach (char ch in text)
            {
                result.Append(enigma.Code(ch));
            }

            return result.ToString();
        }
        /// <summary>
        /// Анклийский и цифры
        /// </summary>
        /// <param name="text"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string CodeB(string text, string key)
        {
            if (string.IsNullOrEmpty(key)) return text;
            Enigma enigma = new Enigma(key, SET_B);
            StringBuilder result = new StringBuilder();

            foreach (char ch in text)
            {
                result.Append(enigma.Code(ch));
            }

            return result.ToString();
        }

    }

}
