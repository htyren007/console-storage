﻿using System;

namespace ConsoleStorage.Data
{

    public static partial class Coder
    {
        private class Rotor
        {
            internal ushort Point;
            internal ushort Incerment = 1;
            internal ushort MaxValue;


            public Rotor(ushort startPoint, ushort incerment = 1, ushort maxValue = 0)
            {
                if (maxValue == 0) MaxValue = ushort.MaxValue;
                else MaxValue = maxValue;
                if (startPoint > MaxValue) throw new ArgumentException("Символ не входит в нужный деапозон.");
                this.Point = startPoint;
                Incerment = incerment;
            }

            internal ushort RotateSimbol(ushort simbol)
            {
                ushort result = (ushort)((simbol + Point) % MaxValue);
                return result;
            }
            internal ushort UnrotateSimbol(ushort simbol)
            {
                ushort result = (ushort)((MaxValue + simbol - Point) % MaxValue);
                return result;
            }

            internal void Rotate() => Point = (ushort)((Incerment + Point) % MaxValue);
        }

    }

}
