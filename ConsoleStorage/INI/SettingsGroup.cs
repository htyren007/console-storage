﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;

namespace ConsoleStorage.INI
{
    public class SettingsGroup
    {
        protected Group group;

        public SettingsGroup(IniData file, string groupname)
        {
            if (!file.Groups.Any(x => x.Name == groupname))
            {
                group = file.GetGroup(groupname);
                CreateDefault();
                file.SaveFile();
            }
            else
            {
                group = file.GetGroup(groupname);
            }
        }

        protected virtual void CreateDefault()
        {
        }

        protected void SetStringSetting(string value, [CallerMemberName] string propertyName = null)
        {
            group.SetString(propertyName, value);
        }

        protected string GetStringSetting([CallerMemberName] string propertyName = null)
        {
            return group.GetString(propertyName);
        }

        protected void SetDoubleSetting(double value, [CallerMemberName] string propertyName = null)
        {
            group.SetReal(propertyName, value);
        }

        protected double GetDoubleSetting([CallerMemberName] string propertyName = null)
        {
            return group.GetReal(propertyName);
        }

        protected void SetBoolSetting(bool value, [CallerMemberName] string propertyName = null)
        {
            group.SetBool(propertyName, value);
        }

        protected bool GetBoolSetting([CallerMemberName] string propertyName = null)
        {
            return group.GetBool(propertyName);
        }
    }
}