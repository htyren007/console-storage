﻿using System;

namespace ConsoleStorage.INI
{
    public class SettingsBase
    {
        private string fileName;
        protected IniData file;

        public SettingsBase(string filename)
        {
            this.fileName = filename;
            file = IniData.LoadFile(fileName);
        }

        protected void SaveSettings() => file.SaveFile();
    }
}