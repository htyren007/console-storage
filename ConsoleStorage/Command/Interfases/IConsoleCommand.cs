﻿namespace ConsoleStorage.Command
{
    public interface IConsoleCommand
    {
        /// <summary>
        /// Выполнить команду
        /// </summary>
        /// <param name="context">Контекст выполнения</param>
        void Run(CommandContext context);

        /// <summary>
        /// Проверка необходимо ли выполнять именно эту команду
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        bool NeedThis(CommandContext context);
    }
}
