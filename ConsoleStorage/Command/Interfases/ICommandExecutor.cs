﻿using System.Threading.Tasks;

namespace ConsoleStorage.Command
{
    public interface ICommandExecutor
    {
        // Task Run();
        // Task Stop();
        void Run();
        void Stop();

        void SetCommands(ICommandCollection commands);
        void RunCommand(CommandContext context);
    }
}