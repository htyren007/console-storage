﻿using System.Collections.Generic;

namespace ConsoleStorage.Command
{
    public interface ICommandCollection
    {
        bool IsWork { get; }

        void WriteNotify();

        IConsoleCommand FindCommand(CommandContext context);
        void CommandNotFound(CommandContext context);
        IEnumerable<TCommand> GetCommands<TCommand>() where TCommand : IConsoleCommand;
    }
}