﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleStorage.Command
{
    public class SubCommand : AConsoleCommand, ICommandCollection
    {
        private List<IConsoleCommand> subCommands = new List<IConsoleCommand>();


        public SubCommand(string commandNames)
        {
            CommandName = commandNames;
            AddSimple(new string[] { "back"}, () => IsWork = false);
        }

        public bool IsWork { get; set; }

        public override void Run(CommandContext context)
        {
            Executor executorCustomer = new Executor();
            executorCustomer.SetCommands(this);
            if (context.Path.Length == 1)
            {
                Line("Режим ", CommandName);
                IsWork = true;
                executorCustomer.Run();
            }
            else if (context.Path.Length > 1)
            {
                var subContext = context.Dublicate();
                subContext.CommandName = context.Path[1];
                subContext.Path = context.Path.Skip(1).ToArray();
                executorCustomer.RunCommand(subContext);
            }
        }

        public void AddSimple(string[] commandNames, Action action)
        {
            if (commandNames.Length == 1)
            {
                Add(new SimpleCommand(commandNames[0], action));
            }
            else
                throw new NotImplementedException();
        }

        public void Add(IConsoleCommand command)
        {
            subCommands.Add(command);
        }

        public virtual void WriteNotify() => Text($"[{CommandName}] Введите команду: ");

        public override bool NeedThis(CommandContext context)
        {
            return base.NeedThis(context) || SubCommandCollection.SplitNameCommand(context.CommandName)[0] == CommandName;
        }

        public IConsoleCommand FindCommand(CommandContext context)
        {
            return context.Command = subCommands.Find(x => x.NeedThis(context));
        }

        public void CommandNotFound(CommandContext context)
        {
            Line("Команда не найдена!");
        }

        public IEnumerable<TCommand> GetCommands<TCommand>() where TCommand : IConsoleCommand
        {
            foreach (var icomand in subCommands)
            {
                if (icomand is TCommand command)
                {
                    yield return command;
                }
            }
        }
    }
}