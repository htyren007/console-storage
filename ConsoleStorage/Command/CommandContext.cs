﻿namespace ConsoleStorage.Command
{
    public class CommandContext
    {
        public string Input;
        public string[] Words;
        public string[] Params;
        public string CommandName;
        public IConsoleCommand Command;
        public string[] Path;

        public CommandContext Dublicate()
        {
            return new CommandContext()
            {
                Input = Input,
                Words = Words,
                Params = Params,
                CommandName = CommandName

            };
        }
    }
}