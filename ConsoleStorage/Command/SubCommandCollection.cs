﻿using System;
using System.Linq;

namespace ConsoleStorage.Command
{
    public class SubCommandCollection : ACommandCollection
    {

        protected void AddSimple(string commandNames, Action action, string displayName = null)
        {
            string[] path = SplitNameCommand(commandNames);
            if (path.Length > 1)
            {
                var name = path[0];

                SubCommand sub = GetCollection(name);

                if (sub == null)
                {
                    sub = new SubCommand(name);
                    commands.Add(sub);
                }

                sub.AddSimple(path.Skip(1).ToArray(), action);
            }
            else
            {
                var command = new SimpleCommand(commandNames, action);
                if (string.IsNullOrEmpty(displayName))
                    command.DisplayName = displayName;
                commands.Add(command);
            }
        }

        public static string[] SplitNameCommand(string commandNames)
        {
            return commandNames.Split('/', ':', '.');
        }

        protected void AddSubCollection(string commandNames, params IConsoleCommand[] subCommands)
        {
            var subCommand = new SubCommand(commandNames);
            commands.Add(subCommand);
            
            foreach (var command in subCommands)
                subCommand.Add(command);
        }

        protected void AddListCommand(string name)
        {
            SubCommand sub = GetCommands<SubCommand>().FirstOrDefault(x => x.CommandName == name);
            var command = new SimpleCommand("list", () => {
                Line("Коллекция", sub.CommandName);
                foreach (var item in sub.GetCommands<AConsoleCommand>())
                {
                    Line(item.CommandName);
                }
            });

            sub.Add(command);
        }

        protected SubCommand GetCollection(string name)
        {
            return GetCommands<SubCommand>().FirstOrDefault(x => x.CommandName == name);
        }
    }
}