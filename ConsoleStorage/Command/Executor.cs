﻿using ConsoleStorage.Utility;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ConsoleStorage.Command
{
    public sealed class Executor : AConsoleWriter, ICommandExecutor
    {
        private bool work = true;
        private ICommandCollection commands;

        public Executor()
        {
        }

        public static bool TryParseCommand(CommandContext context)
        {
            context.Words = CommandHelper.ParseCommands(context.Input.ToLower());
            if (context.Words.Length > 0)
            {
                context.CommandName = context.Words[0];
                context.Params = context.Words.Skip(1).ToArray();
                var path = SubCommandCollection.SplitNameCommand(context.CommandName);
                if (path.Length > 0)
                {
                    context.CommandName = path[0];
                    context.Path = path.ToArray();
                }

                return true;
            }
            return false;
        }

        public void Run()
        {
            work = true;
            while (work)
            {
                commands.WriteNotify();

                string words = ConsoleHelper.Query(" ");
                CommandContext context = new CommandContext()
                {
                    Input = words,
                };
                if (TryParseCommand(context))
                {
                    RunCommand(context);
                }

                if (!commands.IsWork)
                    break;
            }
        }

        public void Stop() => work = false;

        public void SetCommands(ICommandCollection commands) => this.commands = commands;

        public void RunCommand(CommandContext context)
        {
            var command = commands.FindCommand(context);
            if (command != null)
            {
                command.Run(context);
            }
            else 
            {
                commands.CommandNotFound(context);
            }
        }

        public void RunCommand(string text)
        {
            CommandContext context = new CommandContext()
            {
                Input = text,
            };
            if (TryParseCommand(context))
            {
                RunCommand(context);
            }
        }

        public static bool TryParseCommand(string[] words, out string command, out string[] inputs)
        {
            if (words.Length > 0)
            {
                command = words[0];
                inputs = words.Skip(1).ToArray();
                return true;
            }

            command = string.Empty;
            inputs = null;
            return false;
        }
    }
}