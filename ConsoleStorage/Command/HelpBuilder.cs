﻿using System.Linq;
using System.Text;

namespace ConsoleStorage.Command.Help
{
    public class HelpBuilder
    {
        private string name;
        private StringBuilder builder;

        public HelpBuilder(string name)
        {
            this.name = name;
            this.builder = new StringBuilder();
        }

        public void AddVariant(string key, string description)
        {
            builder.Append(name);
            builder.Append(' ');
            builder.Append(key);
            builder.Append(": ");
            builder.AppendLine(description);
        }

        public void AddVariant(string description)
        {
            builder.Append(name);
            builder.Append(": ");
            builder.AppendLine(description);
        }

        public void AddDescription(string description)
        {
            builder.AppendLine(description);
        }

        public override string ToString()
        {
            return builder.ToString();
        }
    }
}