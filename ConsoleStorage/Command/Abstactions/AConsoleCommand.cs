﻿using System;

namespace ConsoleStorage.Command
{
    /// <summary>
    /// Абстрактное описание команды
    /// </summary>
    public abstract class AConsoleCommand : AConsoleWriter, IConsoleCommand
    {
        /// <summary>Описание команды</summary>
        public virtual string Help { get; set; }

        /// <summary>Уникальное имя команды без пробелов</summary>
        public virtual string CommandName { get; set; }

        /// <summary>Человеческое имя</summary>
        public virtual string DisplayName { get; set; }

        /// <summary>Выполняется после выбора команды</summary>
        public abstract void Run(CommandContext context);

        /// <summary>
        /// Подходит ли команда для выполнения
        /// </summary>
        /// <param name="textCommand"></param>
        /// <returns></returns>
        public virtual bool NeedThis(CommandContext context)
        {
            return context.CommandName.Equals(CommandName, StringComparison.CurrentCultureIgnoreCase);
        }
    }
}
