﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleStorage.Command
{
    public abstract class ACommandCollection : AConsoleWriter, IEnumerable<IConsoleCommand>, ICommandCollection
    {
        protected List<IConsoleCommand> commands = new List<IConsoleCommand>();

        /// <summary>
        /// <see cref="Executor"/> продолжает выполнять команды пока <c>true</c> 
        /// </summary>
        public virtual bool IsWork { get; set; } = true;

        /// <summary>
        /// Вызывается перед каждой командой, предназначен для вывода информирующего сообщения
        /// </summary>
        public virtual void WriteNotify() => Text("Введите команду: ");

        public virtual IConsoleCommand FindCommand(CommandContext context)
        {
            return context.Command = commands.Find(x => x.NeedThis(context));
        }

        public IEnumerator<IConsoleCommand> GetEnumerator()
        {
            return ((IEnumerable<IConsoleCommand>)commands).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)commands).GetEnumerator();
        }

        public void ForEach<TCommand>(Action<TCommand> action) where TCommand : IConsoleCommand
        {
            foreach (var icomand in commands)
            {
                if (icomand is TCommand command)
                {
                    action(command);
                }
            }
        }
        public IEnumerable<TCommand> GetCommands<TCommand>() where TCommand : IConsoleCommand
        {
            foreach (var icomand in commands)
            {
                if (icomand is TCommand command)
                {
                    yield return command;
                }
            }
        }

        public void CommandNotFound(CommandContext context)
        {
            Line("Команда '", context.CommandName, "' не найдена");
        }
    }
}