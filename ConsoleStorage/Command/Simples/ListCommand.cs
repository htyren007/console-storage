﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConsoleStorage.Command
{
    public class ListCommand : AConsoleCommand
    {
        private ICommandCollection commands;
        private string[] calls = new string[] { "commands", "com-ls", "ls-com" };
        public ListCommand(ICommandCollection commands)
        {
            this.commands = commands;
            CommandName = "com-ls";
            DisplayName = "Вывести все команды";
            Help = @"Вывести все команды";
        }

        //public override string[] Calls => calls;

            //public override void Run(string[] inputs)
            //{
            //    foreach (var command in commands)
            //    {
            //        Line(command.Calls);
                
            //    }
            //    Space();
            
            //}

        public override void Run(CommandContext context)
        {
            foreach (var icommand in commands.GetCommands<IConsoleCommand>())
            {
                if (icommand is AConsoleCommand command)
                    Line(command.CommandName);
            }
            Space();
        }
    }
}