﻿using System;
using System.Threading.Tasks;

namespace ConsoleStorage.Command
{
    public class SimpleCommand<TInput> : AConsoleCommand
    {
        private string[] calls;
        private Action<TInput> action;
        private Func<string, TInput> convert;

        public SimpleCommand(string commandName, Action<TInput> action, Func<string, TInput> convert)
        {
            CommandName = commandName;
            this.action = action;
            this.convert = convert;
        }

        public override void Run(CommandContext context)
        {
            try
            {
                if (context.Params.Length > 0)
                {
                    action?.Invoke(convert.Invoke(context.Params[0]));
                }
            }
            catch (Exception ex)
            {
                Error("Выполнение команды закончилось ошибкой");
                Line($"{ex.GetType().Name}: {ex.Message}");
            }
        }
    }
}
