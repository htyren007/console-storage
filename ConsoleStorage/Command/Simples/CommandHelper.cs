﻿using System;

namespace ConsoleStorage.Command
{
    public static class CommandHelper
    {
        public static string Join(string[] args)
        {
            throw new NotImplementedException();
        }

        public static string[] ParseCommands(string command)
        {
#if (NETFRAMEWORK || NET48 || NET472 || NET471 || NET47 || NET462 || NET461 || NET46 || NET452 || NET451 || NET45 || NET40 || NET35 || NET20)
            return command.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
#elif (NET || NET6_0 || NET5_0)
            return command.Split(' ', StringSplitOptions.RemoveEmptyEntries);
#endif
        }
    }

}
