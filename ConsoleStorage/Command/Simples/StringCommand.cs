﻿using ConsoleStorage.Utility;
using System;

namespace ConsoleStorage.Command
{
    public class StringCommand : AConsoleCommand
    {
        private Action<string> action;

        public StringCommand(string commandName, Action<string> action)
        {
            CommandName = commandName;
            this.action = action;
        }

        public override void Run(CommandContext context)
        {
            string parametr = null;
            if (context.Params.Length == 1)
                parametr = context.Params[0];
            else if (context.Params.Length > 1)
                parametr = string.Join(" ", context.Params);
            else
                parametr = ConsoleHelper.Query("Введите параметр");

            action?.Invoke(parametr);
        }
    }
}
