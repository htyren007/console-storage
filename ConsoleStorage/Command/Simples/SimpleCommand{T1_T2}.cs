﻿using System;
using System.Threading.Tasks;

namespace ConsoleStorage.Command
{
    public class SimpleCommand<TI1, TI2> : AConsoleCommand
    {
        private Action<TI1, TI2> action;
        private Func<string, TI1> convert1;
        private Func<string, TI2> convert2;

        public SimpleCommand(string commandName, Action<TI1, TI2> action, Func<string, TI1> convert1, Func<string, TI2> convert2)
        {
            CommandName = commandName;
            this.action = action;
            this.convert1 = convert1;
            this.convert2 = convert2;
        }

        public override void Run(CommandContext context)
        {
            if (context.Params.Length > 1)
            {
                try
                {
                    TI1 input1 = Convert1(context.Params[0]);
                    TI2 input2 = Convert2(context.Params[1]);
                    try
                    {
                        action?.Invoke(input1, input2);
                    }
                    catch (Exception ex)
                    {
                        Error("Выполнение команды закончилось ошибкой");
                    }
                }
                catch (Exception ex)
                {
                    Line($"{ex.GetType().Name}: {ex.Message}");
                }
            }
        }

        private TI2 Convert2(string text)
        {
            try
            {
                return convert2.Invoke(text);
            }
            catch 
            {
                Error("Конвертация аргумента 2 закончилась ошибкой");
                throw;
            }
        }

        private TI1 Convert1(string text)
        {
            try
            {
                return convert1.Invoke(text);
            }
            catch 
            {
                Error("Конвертация аргумента 1 закончилась ошибкой");
                throw;
            }
        }
    }
}
