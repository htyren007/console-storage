﻿using ConsoleStorage.Utility;
using System;

namespace ConsoleStorage.Command
{
    public class IntegerCommand : SimpleCommand<int>
    {
        public IntegerCommand(string command, Action<int> action) : base(command, action, (q) => ConvertHelper.StrToInt(q))
        {
        }

    }

    public class BoolCommand : SimpleCommand<bool>
    {
        public BoolCommand(string command, Action<bool> action) : base(command, action, (q) => ConvertHelper.StrToBool(q))
        {
        }

    }
}
