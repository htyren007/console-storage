﻿using System;
using System.Threading.Tasks;

namespace ConsoleStorage.Command
{
    public class SimpleCommand : AConsoleCommand
    {
        private string[] calls;
        private Action action;

        public SimpleCommand(string commandName, Action action)
        {
            CommandName = commandName;
            this.action = action;

        }


        public override void Run(CommandContext context)
        {
            action?.Invoke();
        }
    }

}
