﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConsoleStorage.Command
{
    public class HelpCommand : AConsoleCommand
    {
        private string[] calls = new string[] { "help", "hlp" };
        private ACommandCollection commands;
        public Action<AConsoleCommand, bool> WriteCommand;

        public HelpCommand(ICommandCollection commands)
        {
            this.commands = commands as ACommandCollection;
            CommandName = "help";
            DisplayName = "Получить справку о команде 'help <имя команды>'";
            Help = @"Получить справку о команде или командах
help all            выводит описание о всех командах в системе
help <имя команды>  выводит подробное описание о конкретной команде
help <имя команды> [.. <имя команды>]  выводит подробное описание всех перечисленных команд";
        }

        

        public override void Run(CommandContext context)
        {
            var param = context.Params;
            if (param.Length == 0 || param.Any(x => x.Equals("all")))
            {
                WriteAll();
            }
            else
            {
                WriteHelp(param);
            }

        }

        private void WriteHelp(string[] inputs)
        {
            foreach (var icommand in commands)
            {
                if (icommand is AConsoleCommand command 
                    && inputs.Contains(command.CommandName))
                {
                    if (WriteCommand != null)
                        WriteCommand(command, true);
                    else
                        Write(command, true);
                }
            }
        }

        private void WriteAll()
        {
            foreach (var icommand in commands)
            {
                if (icommand is AConsoleCommand command)
                {
                    if (WriteCommand != null)
                        WriteCommand(command, false);
                    else
                        Write(command, false);
                }
            }
        }

        public virtual void Write(AConsoleCommand command, bool inDetail = false)
        {
            if (inDetail)
            {
                Header(command.CommandName);
                Line("    ", command.Help);
                Space();
            }
            else
                Line($"{command.CommandName, -8} ", command.DisplayName);
        }
    }
}