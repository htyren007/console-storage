﻿using System;
using System.Text;

namespace ConsoleStorage
{
    /// <summary>
    /// Класс пишущий в консоль
    /// </summary>
    public abstract class AConsoleWriter
    {
        /// <summary>
        /// Установить цвета текста и фона
        /// </summary>
        /// <param name="message">сообщение</param>
        protected void SetColors(ConsoleColor foreground, ConsoleColor background)
        {
            Console.ForegroundColor = foreground;
            Console.BackgroundColor = background;
        }

        /// <summary>
        /// Пишет сообщение красными буквами на белом фоне
        /// </summary>
        /// <param name="message">сообщение</param>
        protected void Error(string message)
        {
            SetColors(ConsoleColor.Red, ConsoleColor.Black);
            Console.WriteLine(message);
            Console.ResetColor();
        }

        /// <summary>
        /// Пишет сообщение красными буквами на белом фоне
        /// </summary>
        /// <param name="message">сообщение</param>
        protected void Error(Exception ex, bool needType = true, bool needStackTrace = false)
        {
            if (needType)
            {
            SetColors(ConsoleColor.Red, ConsoleColor.White);
                Console.Write(ex.GetType().Name);
                Console.Write(": ");
            }
            SetColors(ConsoleColor.Black, ConsoleColor.White);
            Console.WriteLine(ex.Message);
            Console.ResetColor();
            if (needStackTrace)
                Console.WriteLine(ex.StackTrace);
        }

        /// <summary>
        /// Пишет сообщение черными буквами на белом фоне
        /// </summary>
        /// <param name="message">сообщение</param>
        protected void Header(string header)
        {
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine(header);
            Console.ResetColor();
        }

        /// <summary>
        /// Пишет сообщение с переносом строки
        /// </summary>
        /// <param name="message">сообщение</param>
        protected void Line(params string[] messages) => Console.WriteLine(string.Join(" ", messages));

        /// <summary>
        /// Пишет сообщение (без переноса строки)
        /// </summary>
        /// <param name="message">сообщение</param>
        protected void Text(string message) => Console.Write(message);

        /// <summary>
        /// Очищает экран
        /// </summary>
        /// <param name="message">сообщение</param>
        protected void Clear() => Console.Clear();

        /// <summary>
        /// Пропускает строку
        /// </summary>
        /// <param name="message">сообщение</param>
        protected void Space() => Console.WriteLine();

        /// <summary>
        /// Устанавливает позицию курсора
        /// </summary>
        /// <param name="left"></param>
        /// <param name="top"></param>
        protected void SetCursorPosition(int left, int top) => Console.SetCursorPosition(left, top);

    }
}
