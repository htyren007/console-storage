﻿using ConsoleStorage.Slides;
using ConsoleStorage.Slides.Forms;
using System;

namespace ConsoleStorage.ConsoleUI
{
    public abstract class UI : CustomForm
    {
        private Screen screen;

        /// <summary>
        /// Добовление элементов на сцену и их форматирование
        /// </summary>
        public abstract void Designing();

        public void Run()
        {
            Designing();
            screen = new Screen();
            screen.Slide = this;
            screen.Run();
        }

        public void Close()
        {
            NextSlide = null;
        }
    }
}
