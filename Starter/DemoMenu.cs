﻿using ConsoleStorage.ConsoleMenu;
using ConsoleStorage.Slides;
using ConsoleStorage.ConsoleUI;

namespace Starter
{
    [SlideTitle("Настройки")]
    internal class DemoMenu
    {
        [SlideButton("DemoForm")]
        public void DemoForm()
        {
            Screen screen = new Screen();
            screen.Slide = new DemoForm();
            screen.Run();
        }

        [SlideButton("DemoOptions")]
        public void DemoOptions()
        {
            var options = new Options<DemoOptions>();
            options.Run();
        }

        [SlideButton("DemoConsoleUI")]
        public void DemoConsoleUI()
        {
            var ui = new DemoUI();
            ui.Run();
        }

        [SlideButton]
        public void Exit(ASlide slide)
        {
            slide.NextSlide = null;
        }
    }
}