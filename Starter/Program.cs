﻿// #define COMMAND

using ConsoleStorage.Command;
using ConsoleStorage.ConsoleMenu;
using ConsoleStorage.Slides;
using System.Threading.Tasks;

namespace Starter
{
    class Program
    {
        static void Main(string[] args)
        {
#if COMMAND
            Executor executor = new Executor();

            // executor.SetCommands(new SqlCommands());
            executor.SetCommands(new DemoCommands());

            if (args.Length > 0)
                executor.RunCommand(CommandHelper.Join(args));

            executor.Run();
#endif
            var ui = new DemoUI();
            ui.Run();
        }
    }
}
