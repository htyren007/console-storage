﻿// #define COMMAND
using ConsoleStorage.ConsoleMenu;
using ConsoleStorage.Slides;
using ConsoleStorage.Slides.Forms;
using System;

namespace Starter
{
    public class DemoForm : Form
    {
        public DemoForm()
        {
            int top = 4;
            int left = 5;

            AddLabel(new Label("Поле 1", left, top++));
            AddElement(new InputField() { Left = left, Top = top++, Content = "Значение", Width = 20 });
            AddLabel(new Label("Поле 2", left, top++));
            AddElement(new InputField() { Left = left, Top = top++, Content = "Значение", Width = 20 });
            top = 4;
            left += 25;
            AddElement(new CheckBox() { Left = left, Top = top++, Content = "CheckBox 1" });
            top++;
            AddElement(new CheckBox() { Left = left, Top = top++, Content = "CheckBox 2" });
            top = 14;
            left = 5;
            AddElement(new Button() { 
                Left = left, 
                Top = top, 
                Content = "Выход", 
                Action = () => NextSlide = null });
        }
    }
}