﻿// #define COMMAND
using ConsoleStorage.ConsoleMenu;
using ConsoleStorage.Slides;

namespace Starter
{
    [SlideTitle("Настройки")]
    public class DemoOptions
    {
        private bool isNew = true;

        [SlideProperty(left: 50, top: 4)]
        public bool IsVisuble { get; set; }
        [SlideProperty(left: 50, top: 5)]
        public bool IsNew { get => isNew; set { isNew = value;Label = isNew ? "Новое" : "Старое";  } }
        [SlideLabel(left:50, top:6)]
        public string Label_Name { get; set; } = "Name";
        [SlideProperty(left:50, top:7)]
        public string Name { get; set; } = "Значение";

        [SlideButton]
        public void Exit(ASlide slide)
        {
            slide.NextSlide = null;
        }

        [SlideButton("Заполнить имя")]
        public void SetName()
        {
            Name = "Иван";
        }

        [SlideLabel]
        public string Label { get; set; } = "";
    }
}