﻿// #define COMMAND

using ConsoleStorage.ConsoleMenu;
using ConsoleStorage.ConsoleUI;
using ConsoleStorage.Slides;
using ConsoleStorage.Slides.Forms;
using System;

namespace Starter
{
    internal class DemoUI : UI
    {
        private TextField title;
        private TextField text_Center;
        private TextField text_Left;
        private TextField text_Right;
        private InputField inputField1;
        private CheckBox checkBox1;
        private Button button1;
        private Button button2;
        private Button button3;
        private Button button4;
        private Button buttonExit;

        public DemoUI()
        {
        }

        public override void Designing()
        {
            #region TextField
            title = new TextField()
            {
                BackgroundNormal = System.ConsoleColor.DarkGray,
                ForegroundNormal = System.ConsoleColor.White,
                Content = "DemoUI",
                Left = 0,
                Top = 0,
                Widht = Console.WindowWidth,
                Alignment = HorizontalTypeAlignment.Center,
            };
            Add(title);

            text_Center = new TextField()
            {
                BackgroundNormal = System.ConsoleColor.DarkMagenta,
                ForegroundNormal = System.ConsoleColor.White,
                Content = "Alignment.Center",
                Left = 0,
                Top = 1,
                Widht = Console.WindowWidth / 2,
                Alignment = HorizontalTypeAlignment.Center,
            };
            Add(text_Center);

            text_Left = new TextField()
            {
                BackgroundNormal = System.ConsoleColor.DarkMagenta,
                ForegroundNormal = System.ConsoleColor.White,
                Content = "Alignment.Left",
                Left = 0,
                Top = 2,
                Widht = Console.WindowWidth / 2,
                Alignment = HorizontalTypeAlignment.Left,
            };
            Add(text_Left);

            text_Right = new TextField()
            {
                BackgroundNormal = System.ConsoleColor.DarkMagenta,
                ForegroundNormal = System.ConsoleColor.White,
                Content = "Alignment.Right",
                Left = 0,
                Top = 3,
                Widht = Console.WindowWidth / 2,
                Alignment = HorizontalTypeAlignment.Right,
            };
            Add(text_Right);
            #endregion

            inputField1 = new InputField() { 
            Top = 5,
            Width = 30,
            Left = 10,
            
            };
            inputField1.ValueChange += InputField1_ValueChange;
            Add(inputField1);

            checkBox1 = new CheckBox()
            {
                Top = 6,
                Width = 30,
                Left = 10,
                Content = "checkBox1",
            };
            checkBox1.ValueChange += CheckBox1_ValueChange; ;
            Add(checkBox1);

            button1 = new Button()
            {
                Top = 7,
                Width = 15,
                Left = 10,
                Content = "button1",
            };
            button1.Action = ActionButton;
            Add(button1);

            button2 = new Button()
            {
                Top = 8,
                Width = 15,
                Left = 5,
                Content = "DemoForm",
            };
            button2.Action = DemoForm;
            Add(button2);

            button3 = new Button()
            {
                Top = 8,
                Width = 15,
                Left = 25,
                Content = "DemoOptions",
            };
            button3.Action = DemoOptions;
            Add(button3);

            button4 = new Button()
            {
                Top = 10,
                Width = 15,
                Left = 5,
                Content = "DemoList",
            };
            button4.Action = DemoOptions;
            Add(button4);

            buttonExit = new Button()
            {
                Top = 15,
                Width = 15,
                Left = 5,
                Content = "Exit",
            };
            buttonExit.Action = Exit;
            Add(buttonExit);
        }

        private void Exit()
        {
            this.Close();
        }

        public void DemoOptions()
        {
            var options = new Options<DemoOptions>();
            options.Run();
        }

        public void DemoForm()
        {
            Screen screen = new Screen();
            screen.Slide = new DemoForm();
            screen.Run();
        }

        int index = 0;
        

        private void ActionButton()
        {
            text_Center.Content = index++.ToString(); 
        }

        private void CheckBox1_ValueChange(bool obj)
        {
            text_Left.Content = obj.ToString();
        }

        private void InputField1_ValueChange(string obj)
        {
            text_Right.Content = obj;
            
        }
    }
}