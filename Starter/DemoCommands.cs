﻿using ConsoleStorage.Command;
using ConsoleStorage.Slides;
using ConsoleStorage.Utility;
using System;

namespace Starter
{
    public class DemoCommands : SubCommandCollection
    {
        public DemoCommands()
        {
            commands.Add(new HelpCommand(this));
            commands.Add(new ListCommand(this));
            commands.Add(new IntegerCommand("intcom", (n) => Line($"IntegerCommand. Введено: {n}!!!")));
            commands.Add(new SimpleCommand<int>("int1", (n) => Line($"SimpleCommand: Введено {n}!!!"), ConvertHelper.StrToInt));
            commands.Add(new SimpleCommand<int, int>("int2", (n1, n2) => Line($"SimpleCommand: Введено {n1}, {n2}!!!"), ConvertHelper.StrToInt, ConvertHelper.StrToInt));

            AddSubCollection("sub", 
                new SimpleCommand("zxc", () => Line("Привет мир! zxc!!!")),
                new SimpleCommand("asd", () => Line("Привет мир! asd!!!"))
                );
            AddSimple("sub:hello", () => Line("Привет мир!!!"));
            AddSimple("sub:qwe", () => Line("Привет мир! qwe!!!"));
            
            Simple("menu", OpenMenu);
            Simple("open", Open);
            Simple("q", Exit);
        }

        public string NotificationText { get; private set; } = "Введите команду:";

        private void Simple(string names, Action action)
        {
            commands.Add(new SimpleCommand(names, action));
        }


        private void Open()
        {
            NotificationText = "Привет! Введите команду:";
        }

        private void Exit()
        {
            IsWork = false;
        }

        private void OpenMenu()
        {

            MenuSlide main = new MenuSlide();
            main.Header.Text = "Starter";
            main.Header.Left = 4;
            var help = new ColorLabel { Text = "", Left = 30, Top = 5 };
            main.AddLabel(help);
            main.AddItem(new MenuItem { Title = "Пункт 1", Action = () => help.Text = "Пункт 1" });
            main.AddItem(new MenuItem { Title = "Пункт 2", Action = () => help.Text = "Пункт 2" });
            main.AddItem(new MenuItem { Title = "BaseDir", Action = () => help.Text = AppContext.BaseDirectory });
            main.AddLabel(new Label("Для выхода нажмите \"Esc\"", 4, 10));

            Screen screen = new Screen();
            screen.Slide = main;
            screen.Run();
        }

        public override void WriteNotify()
        {
            Text(NotificationText);
        }
    }
}
